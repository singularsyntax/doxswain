function doxswain_find_doxfile()
{
    local DIR=$(realpath "$@")

    if [[ -f $DIR/.doxswain ]]
    then
        return 0
    else
        if [[ $DIR != / ]]
        then
            doxswain_find_doxfile $(dirname $DIR)
        else
            return 1
        fi
    fi
}

function doxswain()
{
    case "$1" in
        sail)
            command doxswain $@
            eval $(doxswain alias $PWD) 2>/dev/null
            ;;
        sink)
            command doxswain $@
            eval $(doxswain unalias $PWD) 2>/dev/null
            ;;
        stop)
            command doxswain $@
            eval $(doxswain unalias $PWD) 2>/dev/null
            ;;
        *)
            command doxswain $@
    esac
}

function cd()
{
    doxswain_find_doxfile $PWD

    local OLD_DOX=$?
    local OLD_DIR=$PWD

    builtin cd $@

    if [[ $? -eq 0 ]]
    then
        doxswain_find_doxfile $PWD

        local NEW_DOX=$?
        local NEW_DIR=$PWD

        if [[ $OLD_DOX == 0 ]] || [[ $NEW_DOX == 0 ]]
        then
            eval $(doxswain alias $OLD_DIR $NEW_DIR) 2>/dev/null
        fi
    fi
}

# For the case where a new shell is invoked in an initial directory
# (usually ~) with an already running Doxswain container

cd $PWD
