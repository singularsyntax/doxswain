Doxswain
========

Doxswain is a minimalist container management tool for [Docker](https://www.docker.com/), inspired by [Vagrant](https://www.vagrantup.com/). It runs in any environment which supports [Python 3](https://www.python.org/).

Installation
------------

    make PREFIX=/usr/local install

Doxfile
-------

A Doxswain environment is established by running the `doxswain init` command in the project directory for workflows which use Docker containers for building, testing, and deployment:

    $ doxswain init

This results in a `Doxfile` being created in the current working directory:

    image: singularsyntax/doxswain

`Doxfile`s are composed in YAML. A minimal `Doxfile` needs only to contain an `image` specifier which refers to an image on [Docker Hub](https://hub.docker.com/),
but the `doxswain config` command shows the complete configuration including merged defaults:

    alias:
      - python
    image: python:3.6-alpine3.10
    doxpath: /usr/local/share/doxswain
    options:
      cli:
        command: /bin/ash
        interactive: true
        tty: true
        workdir: /doxswain
      run:
        detach: true
        hostname: localhost.localdomain
        mounts:
        - source: /usr/local/share/doxswain
          target: /doxswain
          type: bind
        name: doxswain._usr_local_share_doxswain
        tty: true

Workflow
--------

Once the environment has been initialized, the following commands can be used:

- Start and run the container:

      $ doxswain sail

- Stop the container:

      $ doxswain stop

- Destroy the container:

      $ doxswain sink

- Check the container status:

      $ doxswain status

Console
-------

TODO

CLI
---

To quickly start a shell inside the container, simply type:

    $ doxswain cli

By default, the shell is `/bin/bash`, but it can be overridden in the `Doxfile`. Suppose you're using an [Alpine Linux](https://hub.docker.com/_/alpine) image, which doesn't have Bash. You can specify an alternative shell like this:

    options:
      cli:
        command: /bin/ash

You can execute `doxswain cli` multiple times to obtain as many shells inside the container as you may need. Another convenience is that the directory containing the `Doxfile` is automatically mounted inside the container at `/doxswain`.

Aliases
-------

A really handy feature of Doxswain is the ability to set up shell aliases on your development machine which will run commands inside a container instead of on the machine itself. Suppose you're doing Java development, but you don't want to use the default JDK shipped with your OS, or you simply want to avoid cluttering your system with more and more software packages. You might create a `Doxfile` something like this:

    image: maven:3.3.9-jdk-8
    alias:
      - mvn

After starting a container with `doxswain sail`, command aliases are available:

    $ doxswain alias
    alias mvn='docker exec --workdir /doxswain b88f1b2d38cba5f96c2ea2c98506039678b1431e32c967b674a57f9cbf6ebfb7 mvn';

You can copy and paste the output into your shell window to activate the aliases. Likewise, you can remove them with:

    $ doxswain unalias
    unalias mvn;

You can now invoke `mvn` in your shell as if it is installed locally on your system:

    $ mvn clean
    [INFO] Scanning for projects...

You can include command arguments as well. For example, if you wanted to preserve the Maven local repository beyond the lifecycle of a single container, you could do the following:

    alias:
      - mvn: mvn -Dmaven.repo.local=/doxswain/.build/m2

Shell Integration
-----------------

Doxswain can be integrated with your command shell so that container aliases are automatically added and removed as you navigate into and out of directories containing `Doxfile`s.

## macOS ##

The integration shell script requires [GNU Coreutils](https://formulae.brew.sh/formula/coreutils). You can install it on your system using the [Homebrew](https://brew.sh/) package manager.

## Bash ##

To integrate with [Bash](https://www.gnu.org/software/bash/), source `shells/doxswain.bash` in your shell initialization file, e.g.:

    # Symlink to distribution shell script
    ln -s $PREFIX/share/doxswain/shells/doxswain.bash ~/.doxswain.bash

    # ~/.bash_profile
    [[ -f ~/.doxswain.bash ]] && source ~/.doxswain.bash

## Zsh ##

To integrate with [Zsh](https://www.zsh.org/): the Bash integration script also works for Zsh. Follow the same steps as above but put in your `~/.zshrc`.

Doxswain Images
---------------

Docker images can be tailored to work closely with Doxswain. An `options` section can be added to an image's labels, and whenever a container
is created from the image, the options it defines will be merged into the `Doxfile` configuration. Because of whitespace significance in YAML,
it is more convenient to express options in JSON. Using again the example of an image based on Alpine Linux, the directive to use an alternate
CLI shell could be included in the `Dockerfile` like this:

    LABEL doxswain='{ "options": { "cli": { "command": "/bin/ash" } } }'

It might be necessary to assert the `privileged` option in the `run` or `cli` section for certain use cases. Whenever Doxswain encounters this
option in an image's `doxswain` label, it will deny execution of the container or command unless explicitly allowed by an override directive in
the top level of the `Doxfile`:

    permit-privileged: true

Command Reference
-----------------

Doxfile Reference
-----------------
