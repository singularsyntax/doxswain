import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="doxswain-singularsyntax",
    version="0.0.1",
    author="Stephen J. Scheck",
    author_email="singularsyntax@gmail.com",
    description="Minimalist container management for Docker development environments",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/singularsyntax/doxswain",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: Apache Software License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.6',
    install_requires=['docker==7.0.0', 'dockerpty==0.4.1', 'pyyaml==6.0.1', 'toolz==0.12.1'],
    data_files=[('shells', ['shells/doxswain.bash'])],
    entry_points={
        'console_scripts': [
            'doxswain=doxswain:shell',
        ],
    },
)
