#NAME=$(shell basename `pwd`)
NAME=doxswain
PREFIX=${NAME}_install

init:
	python3 -m venv .build_env
	.build_env/bin/pip --disable-pip-version-check --quiet install --upgrade setuptools wheel

test:

package: init
	mkdir -p ${NAME}
	cp src/${NAME}.py ${NAME}/__init__.py
	.build_env/bin/python3 setup.py sdist bdist_wheel

install: package
	python3 -m venv ${PREFIX}/share/${NAME}
	${PREFIX}/share/${NAME}/bin/pip --disable-pip-version-check --quiet install --upgrade dist/doxswain_singularsyntax-0.0.1-py3-none-any.whl
	mkdir -p ${PREFIX}/bin
	ln -fs ../share/${NAME}/bin/${NAME} ${PREFIX}/bin/${NAME}

uninstall:
	rm -f ${PREFIX}/bin/${NAME}
	rm -rf ${PREFIX}/share/${NAME}

clean:
	rm -rf build
	rm -rf dist
	rm -rf doxswain_singularsyntax.egg-info
	rm -rf ${NAME}

distclean: clean
	rm -rf .build_env
