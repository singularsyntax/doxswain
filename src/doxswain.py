###
## doxswain.py
##
## Copyright 2020-2022 Stephen J. Scheck
##
## Licensed under the Apache License, Version 2.0 (the "License");
## you may not use this file except in compliance with the License.
## You may obtain a copy of the License at
##
##     http://www.apache.org/licenses/LICENSE-2.0
##
## Unless required by applicable law or agreed to in writing, software
## distributed under the License is distributed on an "AS IS" BASIS,
## WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
## See the License for the specific language governing permissions and
## limitations under the License.
##

import copy
import docker
import dockerpty
import os
import sys
import yaml

from docker.errors import DockerException
from toolz.itertoolz import first
from toolz.dicttoolz import merge_with

sail_default_options = {
  "alias": [],
  "options": {
    "run": {
      "detach": True,
      "hostname": "localhost.localdomain",
      "labels": { "doxswain": "{ \"options\": { \"cli\": { \"command\": \"/bin/bash\", \"workdir\": \"/doxswain\" } } }" },
      "tty": True
    },
    "cli": {
      "command": "/bin/bash",
      "interactive": True,
      "tty": True
    }
  }
}

error_key = {
  "doxswain-container-not-created":  "Container has not been created.",
  "doxswain-container-not-running":  "Container is not running.",
  "doxswain-doxfile-missing":        "No Doxfile in current directory. Run `doxswain init` to initialize a new Doxswain environment.",
  "doxswain-container-status":       "Container {}.",
  "doxswain-container-started":      "Container started successfully.",
  "doxswain-container-stopped":      "Container stopped successfully.",
  "doxswain-container-destroyed":    "Container destroyed successfully.",
  "doxswain-execution-privileged":   "Operation requires privileged execution. To proceed, include `permit-privileged: true` in Doxfile."
}

base_option_subs = {
  "interactive": "stdin_open",
  "memory": "mem_limit",
  "memory-reservation": "mem_reservation",
  "publish": "ports"
}


def run_option_subs():
  return copy.deepcopy(base_option_subs)


def cli_option_subs():
  subs = copy.deepcopy(base_option_subs)
  subs["command"] = "cmd"
  subs["interactive"] = "stdin"
  return subs


def make_result(context, key):
  result = {
    "result": key,
    "message": error_key[key]
  }

  if container_with_id(context):
    result["container-id"] = container_with_id(context).short_id

  return result


def key(dict, key):
  if key in dict:
    return dict[key]


def doxfile_path(path):
  if os.path.isfile(path + "/Doxfile"):
    return path

  if not path == os.path.dirname(path):
    return doxfile_path(os.path.dirname(path))
  else:
    return None


def make_sail_default_options(context):
  default_options = copy.deepcopy(sail_default_options)
  run_options = default_options["options"]["run"]
  cli_options = default_options["options"]["cli"]

  default_options["image"] = context["config"]["image"]
  run_options["mounts"] = [{ "target": "/doxswain", "source": context["doxfile-path"], "type": "bind" }]
  run_options["name"] = "doxswain.{}".format(context["doxfile-path"].replace(os.sep, "_"))
  cli_options["workdir"] = make_container_workdir(context["doxfile-path"], os.getcwd())

  return default_options


def make_context():
  context = {
    "client": docker.from_env(),
    "doxfile-path": doxfile_path(os.getcwd()),
    "config": None,
    "container-id": None
  }

  if context.get("doxfile-path"):
    context["config"] = read_yaml_file(context["doxfile-path"] + "/Doxfile")
    context["container-id"] = read_yaml_file(context["doxfile-path"] + "/.doxswain").get("container-id")
    context["config"]["doxpath"] = context["doxfile-path"]

  return context


def with_doxfile(command, context, args):
  if not context.get("doxfile-path"):
    raise Exception(make_result(context, "doxswain-doxfile-missing"))

  return command(context, args)


def with_running_container(command, context, args):

  def _command(context, args):
    if not container_with_id(context):
      raise Exception(make_result(context, "doxswain-container-not-running"))

    return command(context, args)

  return with_doxfile(_command, context, args)


def check_privileged(context, config, container_options):
  if container_options.get("privileged") and not config.get("permit-privileged"):
    raise Exception(make_result(context, "doxswain-execution-privileged"))


def read_yaml_file(file_path):
  try:
    with open(file_path) as file:
      return yaml.safe_load(file.read())
  except FileNotFoundError as e:
    return {}


def print_help():
  print("Usage: doxswain [options] <command> [<args>]")
  print()
  print("Commands:")
  print()
  print("    alias      Print shell commands to create aliases for commands to run in container.")
  print("    unalias    Print shell commands to delete aliases for commands to run in container.")
  print("    cli        Start a CLI (shell) inside the container.")
  print("    config     Print the environment configuration.")
  print("    console    Attach to the root process of the container.")
  print("    init       Initialize a new Doxswain environment by creating a Doxfile.")
  print("    sail       Create and start a container.")
  print("    sink       Destroy the container.")
  print("    stop       Stop the container.")
  print("    status     Show status of the container.")


def docker_opts_to_py_docker_opts(subs, docker_opts):
  py_docker_opts = copy.deepcopy(docker_opts)
  for opt in subs.keys():
    if opt in docker_opts:
      py_docker_opts[subs[opt]] = docker_opts[opt]
      del py_docker_opts[opt]

  return py_docker_opts


def get_container_opts(context):
  labels = context["client"].containers.get(context["container-id"]).labels
  if "doxswain" in labels:
    container_opts = yaml.safe_load(labels.get("doxswain"))
    if "options" in container_opts:
      if "cli" in container_opts["options"]:
        return docker_opts_to_py_docker_opts(run_option_subs(), container_opts["options"]["cli"])

  return docker_opts_to_py_docker_opts(run_option_subs(), get_config_options(sail_default_options, "cli"))


def get_image_opts(context):
    labels = context["client"].images.get(context["config"]["image"]).labels
    return yaml.safe_load(labels["doxswain"]) if "doxswain" in labels else {}


def get_config_options(config, opt_key):
    return config["options"][opt_key]


def container_with_id(context):
    return context["client"].containers.get(context["container-id"]) if context["container-id"] else None


def make_container_workdir(doxfile_path, cwd):
    relpath = os.path.relpath(cwd, doxfile_path)
    return "/doxswain" + ("" if "." == relpath else "/" + relpath)


def alias(context, args):
    aliases = []

    if len(args) == 1:
        doxpath = doxfile_path(args[0])
        if doxpath:
            config = read_yaml_file(doxpath + "/" + "Doxfile")
            if "alias" in config:
                aliases = config["alias"]
    elif len(args) == 2:
        unalias(context, [args[0]])
        alias(context, [args[1]])
    else:
        container = container_with_id(context)
        if container and "running" == container.status:
            if "alias" in context["config"]:
                aliases = context["config"]["alias"]

    for a in aliases:
        container_id = context["container-id"] if context["container-id"] else "<container-id>"
        key = a if isinstance(a, str) else first(a)
        val = a if isinstance(a, str) else a[key]
        dir = make_container_workdir(context["doxfile-path"], os.getcwd())
        print("alias {}='docker exec --workdir {} {} {}';".format(key, dir, container_id, val))

    return {"stdout": True}


def unalias(context, args):
    aliases = []

    if len(args) > 0:
        doxpath = doxfile_path(args[0])
        if doxpath:
            config = read_yaml_file(doxpath + "/" + "Doxfile")
            if "alias" in config:
                aliases = config["alias"]
    else:
        container = container_with_id(context)
        if container and "running" == container.status:
            if "alias" in context["config"]:
                aliases = context["config"]["alias"]

    for alias in aliases:
        key = alias if isinstance(alias, str) else first(alias)
        print("unalias {};".format(key))

    return {"stdout": True}


def init(context, arg):
    if not os.path.isfile("Doxfile"):
        with open("Doxfile", "w") as file:
            file.write("image: {}\n".format(arg if arg else "singularsyntax/doxswain"))

    return {"stdout": True}


def cli(context, args):
    docker_api = context["client"].api
    container_id = context["container-id"]
    config = make_config(context)
    container_options = docker_opts_to_py_docker_opts(cli_option_subs(), get_config_options(config, "cli"))
    check_privileged(context, config, container_options)
    dockerpty.pty.PseudoTerminal(docker_api, dockerpty.pty.ExecOperation(docker_api, docker_api.exec_create(container=container_id, **container_options))).start()

    return {"stdout": True}


def make_mounts(mounts):
    return [docker.types.Mount(**mounts)]


def is_dict(object):
    return isinstance(object, dict)


def deep_merge(vals):
    return merge_with(deep_merge, *vals) if all(map(is_dict, vals)) else vals[0]


def make_config(context):
    return merge_with(deep_merge, context["config"], get_image_opts(context), make_sail_default_options(context))


def sail(context, args):
    config = make_config(context)
    container_options = docker_opts_to_py_docker_opts(run_option_subs(), get_config_options(config, "run"))

    check_privileged(context, config, container_options)

    if context["container-id"]:
        container_with_id(context).start()
    else:
        container = context["client"].containers.run(config["image"], **container_options)
        with open("{}/.doxswain".format(context["doxfile-path"]), "w") as file:
            file.write("container-id: {}\n".format(container.id))

    return make_result(make_context(), "doxswain-container-started")


def sink(context, args):
    stop(context, args)
    container_with_id(context).remove()
    os.remove("{}/.doxswain".format(context["doxfile-path"]))

    return {
        "container-id": context["container-id"],
        "result": "doxswain-container-destroyed",
        "message": error_key["doxswain-container-destroyed"]
    }


def stop(context, args):
    container_with_id(context).stop()
    return make_result(context, "doxswain-container-stopped")


def status(context, args):
    container = container_with_id(context)
    result = make_result(context, "doxswain-container-status")

    if container:
        result["message"] = result["message"].format(container.status)
        result["result"] = "{}-{}".format("doxswain-container-status", container.status)
    else:
        result = make_result(context, "doxswain-container-not-created")

    return result


def _doxswain(args):
    if len(args) == 1:
        print_help()
        exit()

    try:
        context = make_context()
        command = args[1]
        command_args = args[2:None:None]

        if command == "alias":
            return alias(context, command_args) if not len(command_args) == 0 else with_doxfile(alias, context, command_args)
        elif command == "unalias":
            return unalias(context, command_args) if not len(command_args) == 0 else with_doxfile(unalias, context, command_args)
        elif command == "init":
            return init(context, command_args[0] if not len(command_args) == 0 else None)
        elif command == "cli":
            return with_running_container(cli, context, command_args)
        elif command == "config":
            return with_doxfile(lambda context, command_args: make_config(context), context, command_args)
        elif command == "sail":
            return with_doxfile(sail, context, command_args)
        elif command == "sink":
            return with_running_container(sink, context, command_args)
        elif command == "stop":
            return with_running_container(stop, context, command_args)
        elif command == "status":
            return with_doxfile(status, context, command_args)
        else:
            print("'{}' is not a doxswain command.".format(command))
            exit(1)

    except DockerException as e:
        return {"result": "docker-client-error", "message": str(e)}

    except Exception as e:
        return e.args[0]


def doxswain(args):
    result = _doxswain(args)
    return print(yaml.dump(result), end="") if not "stdout" in result else None


def shell():
    return doxswain(sys.argv)


if __name__ == "__main__":
    exit_code = (lambda *args: doxswain(args))(*sys.argv)
    sys.exit(exit_code) if isinstance(exit_code, int) else None
